/************************************************************************
 * This file has been written for the purpose of courses given at the
 * Edinburgh Parallel Computing Centre. It is made freely available with
 * the understanding that every copy of this file must include this
 * header and that EPCC takes no responsibility for the use of the
 * enclosed teaching material.
 *
 * Author:      Joel Malard
 *
 * Contact:     epcc-tec@epcc.ed.ac.uk
 *
 * Purpose:     Bare sequential program for the case study.
 *
 * Contents:    C source code.
 *
 ************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <mpi.h>

/***********************************************************************
 *
 * This file contains C code for modelling a simple predator-prey model.
 * The two animal populations modelled are rabbits and foxes that live on
 * some piece of land. The animal populations are represented by two two-
 * dimensional arrays of size NS_Size by WE_Size. Precisely, the number of
 * foxes that live in the i,j-th stretch of land (with 1<=i<=NS_Size and
 * 1<=j<=WE_Size) is stored as Fox[i][j]. The corresponding figure for
 * rabbits is stored in Rabbit[i][j]. Boundary conditions are enforced so
 * that the land is actually a torus and can be thought to extend
 * periodically beyond the given bounds NS_Size and WE_Size.  The
 * populations of each stretch is updated at each generation according to
 * some simple rules and the total populations are summed at regular
 * intervals of time. The correspondence between array indices, process
 * coordinates and cardinal directions are shown in the diagrams below
 * with arrows pointing in the direction of increasing value.
 *
 *
 *       +-----J----->
 *       |
 *       |                      +--> East
 *       I  A[I][J]             |
 *       |                      V
 *       |                    South
 *       V
 *
 *
 * Procedures and their roles:
 *****************************
 *
 * SetLand: defines a geometric partitioning of the problem.
 *          and initialises the local rabbit and fox populations.
 * Evolve:  Is called repeatedly to compute the next generation
 *          of both foxes and rabbits. Evolve calls the function
 *          FillBorder to enforce the boundary conditions.
 * GetPopulation: Computes the total population of the specie
 *          it is given as argument.
 * FillBorder: Does the actual halo data swaps for the species it
 *          is given has argument.
 *
 ***********************************************************************/

/* Constants and array bounds */
#include "param.h"

/* A few useful macros. */
#include "macro.h"

/* -- Arrays -- */
/* The arrays storing the number of animals in each stretch of land
 * have borders that are used to store halo data thus simplifying the
 * coding of periodic conditions. The actual data for each animal
 * population is stored in columns 1 through WE_Size and in rows 1 through
 * NS_Size of the arrays below.  from nearest neighbour The halo data is
 * stored in rows 0 and NS_Size+1 and in columns 0 and WE_Size+1 of those
 * arrays.
 */
float *Rabbit;
float *Fox;

/* The next two arrays are used in function Evolve() to compute
 * the next generation of rabbits and foxes.
 */
float *TRabbit;
float *TFox;

int size_NS;
int size_WE;

MPI_Comm cart_comm;
MPI_Datatype column, row;
int rank;

/* Function Prototypes */
int SetLand (float *Rabbit, float *Fox, float model[2][3], int *offX, int *offY);
int Evolve (float *Rabbit, float *Fox, float model[2][3]);
int FillBorder (float *Animal);
int GetPopulation (float *Animal, float *tcount);
int *GetDims(int tam);
int *GetSize (int total, int nums, int pos);
void print_matrix(float *Animal);

/* Main Loop */
int main (int argc, char *argv[])
{
	/* -- Local Arrays -- */
	/* The array named model is used to pass the model parameters
		 * across procedures. */
	float model[2][3];

	/* Loop indices, bounds and population counters */
	int k;
	float nbrab, nbfox;
	double t1, t2;
	t1 = t2 = 0.0;
	int err;

	MPI_Init(&argc, &argv);
	
	/* Process rank and number of processes */
	int pid, ntasks;
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);
	MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

	if(pid == 0) {
		t1 = MPI_Wtime();
	}

	/* Cretion of the cartesian communicator*/
	int ndims = 2,*dims = GetDims(ntasks);
#if DEBUG
	printf("dims: %d, %d\n", dims[0], dims[1]);
#endif
	int periods[2] = {TRUE, TRUE}, reorder = TRUE;
	MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, reorder, &cart_comm);

	/* Get the rank and the cartesian coordinates */
	int coords[2];
	MPI_Comm_rank(cart_comm, &rank);
	MPI_Cart_coords(cart_comm, rank, ndims, coords);
#if DEBUG
	printf("coords: %d, %d\n", coords[0], coords[1]);
#endif
	/* Start and end position of world Matrix */
	int *offset_WE = GetSize(WE_Size, dims[1], coords[1]);
	int *offset_NS = GetSize(NS_Size, dims[0], coords[0]);
#if DEBUG
	printf("NS: %d to %d\n", offset_NS[0], offset_NS[1]);
	printf("WE: %d to %d\n", offset_WE[0], offset_WE[1]);
#endif
	/* Size off local Matrix  */
	size_NS = offset_NS[1] - offset_NS[0];
	size_WE = offset_WE[1] - offset_WE[0];
#if DEBUG
	printf("Size: %d x %d\n", size_NS, size_WE);
#endif

	/* Set new row datatype */
	MPI_Type_contiguous(size_WE, MPI_FLOAT, &row);
	MPI_Type_commit(&row);
	/* Set new column datatype */
	MPI_Type_vector(size_NS, 1, size_WE+2, MPI_FLOAT, &column);
	MPI_Type_commit(&column);

	/* Local matrix allocation */
	Rabbit = malloc((size_NS + 2)*(size_WE + 2) * sizeof(float));
	Fox    = malloc((size_NS + 2)*(size_WE + 2) * sizeof(float));

	TRabbit = malloc((size_NS + 2)*(size_WE + 2) * sizeof(float));
	TFox    = malloc((size_NS + 2)*(size_WE + 2) * sizeof(float));

	/* Initialise the problem. */
	err = SetLand(Rabbit, Fox, model, offset_NS, offset_WE);
#if DEBUG
	print_matrix(Fox);
#endif
	/* Iterate. */
	for (k = 1; k <= NITER; k++) {

		err = Evolve(Rabbit, Fox, model);
#if DEBUG
		print_matrix(Fox);
#endif
		if (!(k % PERIOD)) {
			err = GetPopulation(Rabbit, &nbrab);
			err = GetPopulation(Fox, &nbfox);
			/* Reduce all local matrix sub-populations count to global count*/
			float resRab, resFox, tam = 1;
			MPI_Reduce(&nbrab, &resRab, tam, MPI_FLOAT, MPI_SUM, 0, cart_comm);
			MPI_Reduce(&nbfox, &resFox, tam, MPI_FLOAT, MPI_SUM, 0, cart_comm);

			/* Only print in one process */
			if (rank == 0) {
				printf("Year %d: %.0f rabbits and %.0f foxes\n", k, resRab, resFox);
			}
		}
	}
	if(pid == 0) {
		t2 = MPI_Wtime();
		printf("Wall Time: %f\n", t2-t1);
	}
	
	MPI_Finalize();

	return(err);
}
/***********************************************************************
 *
 * Initialise the populations of foxes and rabbits.
 *
 ***********************************************************************/
int SetLand (float *Rabbit, float *Fox, float model[2][3], int *off_NS, int *off_WE)
{
	int err;
	int gi, gj;

	err = 0;
#if DEBUG
	printf("NS Land: %d ate %d\n", off_NS[0], off_NS[1]);
	printf("WE Land: %d ate %d\n", off_WE[0], off_WE[1]);
#endif
	/* Set the parameters of the predator-prey model. */
	model[RABBIT][SAME] = -0.2;
	model[RABBIT][OTHER] = 0.6;
	model[RABBIT][MIGRANT] = 0.01;
	model[FOX][OTHER] = 0.6;
	model[FOX][SAME] = -1.8;
	model[FOX][MIGRANT] = 0.02;

	/* Fill the arrays for foxes and rabbits. */
	for (gj = 1 + off_WE[0]; gj <= off_WE[1]; gj++) {
		for (gi = 1 + off_NS[0];  gi <= off_NS[1]; gi++) {
			Rabbit[AT(gi-off_NS[0],gj-off_WE[0])] =
					128.0 * (gi - 1) * (NS_Size - gi) * (gj - 1) * (WE_Size - gj) /
					(float) (NS_Size * NS_Size * WE_Size * WE_Size);
			Fox[AT(gi-off_NS[0],gj-off_WE[0])] =
					8.0 * (gi / (float)(NS_Size) - 0.5) * (gi / (float)(NS_Size) - 0.5) +
					8.0 * (gj / (float)(WE_Size) - 0.5) * (gj / (float)(WE_Size) - 0.5);
		}
	}

	/* Return the error code. */
	return(err);

}
/***********************************************************************
 *
 * Compute the next generation of foxes and rabbits.
 *
 ***********************************************************************/
int Evolve (float *Rabbit, float *Fox, float model[2][3])
{
	int err;
	int gi, gj;
	float AlR, BtR, MuR, AlF, BtF, MuF;

	err = 0;

	AlR = model[RABBIT][SAME];
	BtR = model[RABBIT][OTHER];
	MuR  = model[RABBIT][MIGRANT];
	BtF = model[FOX][SAME];
	AlF = model[FOX][OTHER];
	MuF  = model[FOX][MIGRANT];

	/* Fill-in the border of the local Rabbit array. */
	err = FillBorder(Rabbit);

	/* Fill-in the border of the local Fox array. */
	err = FillBorder(Fox);

	/* Update the local population data. */
	for (gj = 1; gj <= size_WE; gj++) {
		for (gi = 1; gi <= size_NS; gi++) {
			TRabbit[AT(gi,gj)] = (1.0 + AlR - 4.0 * MuR) * Rabbit[AT(gi,gj)] +
					BtR * Fox[AT(gi,gj)] +
					MuR * (Rabbit[AT(gi,gj-1)] + Rabbit[AT(gi,gj+1)] +
					Rabbit[AT(gi-1,gj)] + Rabbit[AT(gi+1,gj)]);
			TFox[AT(gi,gj)] = AlF * Rabbit[AT(gi,gj)] +
					(1.0 + BtF - 4.0 * MuF) * Fox[AT(gi,gj)] +
					MuF * (Fox[AT(gi,gj-1)] + Fox[AT(gi,gj+1)] +
					Fox[AT(gi-1,gj)] + Fox[AT(gi+1,gj)]);
		}
	}

	/* Ensure the numbers in Rabbit and Fox are non-negative. */
	for (gj = 1; gj <= size_WE; gj++) {
		for (gi = 1; gi <= size_NS; gi++) {
			Rabbit[AT(gi,gj)] = MAX(0.0, TRabbit[AT(gi,gj)]);
			Fox[AT(gi,gj)] = MAX(0.0, TFox[AT(gi,gj)]);
		}
	}

	/* Return the error code. */
	return(err);
}
/***********************************************************************
 *
 * Set the margin of one of the local data array.
 *
 ***********************************************************************/
int FillBorder(float *Animal)
{
	int err;

	err = 0;
	/* Request Handlers */
	MPI_Request reqs[8] = {
		MPI_REQUEST_NULL, MPI_REQUEST_NULL,
		MPI_REQUEST_NULL, MPI_REQUEST_NULL,
		MPI_REQUEST_NULL, MPI_REQUEST_NULL,
		MPI_REQUEST_NULL, MPI_REQUEST_NULL
	};

	/* Get neighboring cells (N,S,E,W) */
	int viz[4];
	MPI_Cart_shift(cart_comm, 0, 1, &viz[NORTH], &viz[SOUTH]);
	MPI_Cart_shift(cart_comm, 1, 1, &viz[WEST], &viz[EAST]);
#if DEBUG
	printf("viz: %d, %d, %d, %d\n", viz[NORTH], viz[SOUTH], viz[WEST], viz[EAST]);
#endif

	/* Set column pointer*/
	float *WE_send = &Animal[AT(1,1)];
	float *EA_send = &Animal[AT(1,size_WE)];

	float *EA_recv = &Animal[AT(1,size_WE+1)]; 
	float *WE_recv = &Animal[AT(1,0)];

	/* Set row pointers */
	float *SO_send = &Animal[AT(size_NS,1)];
	float *NO_send = &Animal[AT(1,1)];

	float *NO_recv = &Animal[AT(0,1)];
	float *SO_recv = &Animal[AT(size_NS+1,1)];

	/* Eastward and westward data shifts. */
	MPI_Isend(WE_send, 1, column, viz[WEST], 0, cart_comm, &reqs[0]);
	MPI_Isend(EA_send, 1, column, viz[EAST], 1, cart_comm, &reqs[1]);

	MPI_Irecv(EA_recv, 1, column, viz[EAST], 0, cart_comm, &reqs[2]);
	MPI_Irecv(WE_recv, 1, column, viz[WEST], 1, cart_comm, &reqs[3]);

	/* Northward and southward data shifts. */
	MPI_Isend(SO_send, 1, row, viz[SOUTH], 2, cart_comm, &reqs[4]);
	MPI_Isend(NO_send, 1, row, viz[NORTH], 3, cart_comm, &reqs[5]);

	MPI_Irecv(NO_recv, 1, row, viz[NORTH], 2, cart_comm, &reqs[6]);
	MPI_Irecv(SO_recv, 1, row, viz[SOUTH], 3, cart_comm, &reqs[7]);
	
	/* Synchronize all Send and Recvs */
	MPI_Waitall(8, reqs, MPI_STATUSES_IGNORE);

	/* Return the error code. */
	return(err);
}
/***********************************************************************
 *
 * Compute the number of individuals in one animal population.
 *
 ***********************************************************************/
int GetPopulation (float *Animal, float *tcount)
{
	int err;
	int i, j;
	float p;

	err = 0;

	/* Sum population. */
	p = 0.0;
	for (j = 1; j <= size_WE; j++)
		for (i = 1; i <= size_NS; i++)
			p = p + Animal[AT(i,j)];

	*tcount = p;

	/* Return the error code. */
	return(err);
}
/***********************************************************************
 *
 * Get the local matrix dimensions for each process
 * 
 ***********************************************************************/
int *GetDims(int tam)
{
	int *divs = malloc(tam*sizeof(int));
	int *res = malloc(2*sizeof(int));

	int i, j = 0;

	/* Get divisibles by 'tam' */
	for (i = 1; i <= tam; i++) {
		if (tam % i == 0) {
			divs[j] = i;
			j++;
		}
	}
	/* Median (most evenly distributed) of the divisibles  */
	res[0] = res[1] = divs[j/2];
	/* not perfect square*/
	if(j % 2 == 0)
		res[1] = divs[j/2 - 1];

	free(divs);

	return res;
}
/***********************************************************************
 *
 * Get location of each proccess in the World Matrix
 * 
 ***********************************************************************/
int *GetSize (int total, int nums, int pos)
{
	/* Array with begin and end*/
	int *res = malloc(2 * sizeof(int));
	res[1] = total / nums;

	int mod = total % nums;
	if (pos < mod) {
		res[0] = pos * (res[1] + 1);
		res[1]++;
	} else {
		res[0] = mod * (res[1] + 1) + (pos - mod) * res[1];
	}
	res[1] = res[0] + res[1];
	return res;
}
/***********************************************************************
 *
 * Print the local Matrix including the borders
 * 
 ***********************************************************************/
void print_matrix(float *Animal)
{
	int i, j;
	for (i = 0; i <= size_NS+1; i++) {
		for (j = 0; j <= size_WE+1; j++) {
			printf("%.3f ", Animal[AT(i,j)]);
		}
		printf("\n");
	}
	printf("\n");
}

