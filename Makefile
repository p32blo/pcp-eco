
PROJ_NAME = eco
np = 2

SOURCE = n2fox.c
HEADER = param.h macro.h

FLAGS = -Wall -O3
DFLAGS = -g -DDEBUG
PFLAGS = -L /share/apps/mpiP-3.4.1 -lmpiP -lm -lbfd -liberty -lunwind

.PHONY : all release debug run clean profile

all: release

release: $(PROJ_NAME)

debug: FLAGS += $(DFLAGS)
debug: $(PROJ_NAME)

profile: FLAGS += $(PFLAGS)
profile: $(PROJ_NAME)

$(PROJ_NAME) : $(SOURCE) $(HEADER)
	mpicc $(SOURCE) -o $(PROJ_NAME) $(FLAGS)

run: $(PROJ_NAME)
	mpirun -np $(np) $(PROJ_NAME)

clean:
	rm -f $(PROJ_NAME)

